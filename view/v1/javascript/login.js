// 登录窗体

class LogWin extends React.Component {
  constructor(props) {
    super(props)
    this.loginActionPath = "http://localhost:4343/login"
    this.state = { show: false }
    this.style = {
        logWinBack: {
          position: 'fixed',
          width: '100%',
          height: '100%',
          top: 0,
          display: 'block'
        },

        logWin: {
          width: '700px',
          height: '500px',
          position: 'absolute',
          top: 0,
          backgroundColor: '#fff',
          borderRadius: '5px',
          margin: '10% 0 0 20%'
        },

        closeLogin: {
          position: 'absolute',
          right: 10,
          top: 5,
          fontSize: '20px'
        },

        logBackgroundWin: {
          width: '100%',
          height: '100%',
          backgroundColor: 'rgba(0, 0, 0, 0.7)'
        }
      }
    }

  render() {
    return (
      <div id="logWinBack" style={this.style.logWinBack}>
        <div style={this.style.logBackgroundWin} onClick={this.closeLoginWin.bind(this)}>
        </div>
        <div id="logWin" style={this.style.logWin}>
          <i id="closeLoginWin" className="iconfont loginCloseX" style={this.style.closeLogin} onClick={this.closeLoginWin.bind(this)}>&#xe613;</i>
          <form id="loginForm" method="POST" action={this.loginActionPath}>
            <LogDiv />
          </form>
        </div>
      </div>
    )
  }

  closeLoginWin(e) {
    this.setState({ show: false })
    this.style.logWinBack.display = "none"
  }
}


class LogDiv extends React.Component {
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <div className="loginArea">
        <div className="login-input-area">
          <span className="ipt-prefix-name ipt-em">用户名：</span>
          <input className="ipt-em ipt-win" type="text" name="username" />
        </div>
        <div className="login-input-area">
          <span className="ipt-prefix-name ipt-em">密码：</span>
          <input className="ipt-em ipt-win" type="password" name="password" />
        </div>
        <LogSubmitButton />
      </div>
    )
  }
}


class LogSubmitButton extends React.Component {
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <div>
        <button className="subBut" type="submit" form="loginForm">登录</button>
      </div>
    )
  }
}



// 动态插入登录窗体样式表
ReactDOM.render(
  <link rel="stylesheet" href="style/login.css" />,
  document.getElementById('extraSheet')
)



$(document).ready(function() {
  $('#clickLogin').click(function() {
    ReactDOM.render(
      <LogWin />,
      document.getElementById('logWinPos')
    )
  });
});
